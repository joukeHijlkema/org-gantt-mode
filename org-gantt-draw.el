(defvar og-schemas '())

;;|--------------------------------------------------------------
;;|Description : create a Gantt chart from an org subtree
;;|NOTE : This is the complete svg version
;;|-
;;|Author : jouke hylkema
;;|date   : 18-56-2020 13:56:06
;;|--------------------------------------------------------------
(defun og-makeGantt (scale W P)
  "create a Gantt chart from an org subtree"
  (interactive "P")
  (let* ((H (* (alist-get 'rowH og-Dims) (+ (length og-project) (alist-get 'headRows og-Dims))))
         (W1 (* P W))
         (W2 (- W W1))
         ;; (total (ts-diff og-end og-start))
         (total (ts-diff og-printEnd og-printStart))
         (svg (svg-create W H
                          :stroke-width 1
                          :fill "none"
                          :stroke-color (alist-get 'strokeColor og-Cols)
                          :font-size  (alist-get 'fontSize og-Text)
                          :dominant-baseline "middle"
                          :font-family  (alist-get 'fontFamily og-Text)))
         (og-schemas '())
         )
    (if og-bg-color (svg-rectangle svg 0 0 W H :fill-color og-bg-color))
    (og-title svg W1 (* (alist-get 'rowH og-Dims) (alist-get 'headRows og-Dims)))
    (og-gridHead svg W1 W2 (* (alist-get 'rowH og-Dims) (alist-get 'headRows og-Dims)) scale total)
    (cl-loop for task being the elements of og-project using (index row) do (og-draw-task svg task row W1 W2))
    (cl-loop for task in og-project do (og-draw-links svg task))
    (og-draw-today svg W1 W2 H)
    (cl-loop for s being the elements of og-schemas do (og-schema s svg))
    (with-temp-file (format "%s.svg" og-svgPath)
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print svg)
      )  
    )
  )
;;|--------------------------------------------------------------
;;|Description : create different svg elements to be included in the html report
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-54-2023 10:54:22
;;|--------------------------------------------------------------
(defun og-makeHTMLelts (W P)
  "create different svg elements to be included in the html report"
  (interactive "P")
  (let* ((H1 (* (alist-get 'rowH og-Dims) (alist-get 'headRows og-Dims)))
         (H2 (* (alist-get 'rowH og-Dims) (length og-project)))
         (H (+ H1 H2))
         (W1 (* P W))
         (W2 (- W W1))
         (total (ts-diff og-printEnd og-printStart))
         (svg_title (svg-create W1 H1
                                :id "svgTitleId"
                                :viewBox (format "0 0 %s %s" W1 H1)
                                :preserveAspectRatio "none"
                                :stroke-width 1
                                :fill "none"
                                :stroke-color (alist-get 'strokeColor og-Cols)
                                :font-size  (alist-get 'fontSize og-Text)
                                :dominant-baseline "middle"
                                :font-family  (alist-get 'fontFamily og-Text)))
         (svg_head (svg-create W2 H1
                               :id "svgHeadId"
                               :viewBox (format "0 0 %s %s" W2 H1)
                               :preserveAspectRatio "none"
                               :stroke-width 1
                               :fill "none"
                               :stroke-color (alist-get 'strokeColor og-Cols)
                               :font-size  (alist-get 'fontSize og-Text)
                               :dominant-baseline "middle"
                               :font-family  (alist-get 'fontFamily og-Text)))
         (svg_tasks (svg-create W1 H2
                                :id "svgTasksId"
                                :viewBox (format "0 0 %s %s" W1 H2)
                                :preserveAspectRatio "none"
                                :stroke-width 1
                                :fill "none"
                                :stroke-color (alist-get 'strokeColor og-Cols)
                                :font-size  (alist-get 'fontSize og-Text)
                                :dominant-baseline "middle"
                                :font-family  (alist-get 'fontFamily og-Text)))
         (svg_gantt (svg-create W2 H2
                                :id "svgGanttId"
                                :viewBox (format "0 0 %s %s" W2 H2)
                                :preserveAspectRatio "none"
                                :stroke-width 1
                                :fill "none"
                                :stroke-color (alist-get 'strokeColor og-Cols)
                                :font-size  (alist-get 'fontSize og-Text)
                                :dominant-baseline "middle"
                                :font-family  (alist-get 'fontFamily og-Text)))
        (og-schemas '())
        )
    (og-title svg_title W1 (* (alist-get 'rowH og-Dims) (alist-get 'headRows og-Dims)))
    (og-gridHead svg_head 0 W2 (* (alist-get 'rowH og-Dims) (alist-get 'headRows og-Dims)) 2 total)
    (cl-loop for task being the elements of og-project using (index row) do
             (og-task-left svg_tasks task row W1 0)
             (og-task-right svg_gantt task row 0 W2 0)
             )
    (cl-loop for task in og-project do (og-draw-links svg_gantt task))
    (cl-loop for s being the elements of og-schemas do (og-schema s svg_tasks t))
    ;; (message "%s" og-htmlPath)
    (mkdir og-htmlPath t)
    (with-temp-file (format "%s/Gantt_title.svg" og-htmlPath)
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print svg_title)
      )
    (with-temp-file (format "%s/Gantt_head.svg" og-htmlPath)
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print svg_head)
      )
    (with-temp-file (format "%s/Gantt_tasks.svg" og-htmlPath)
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print svg_tasks)
      )
    (with-temp-file (format "%s/Gantt_gantt.svg" og-htmlPath)
      (set-buffer-multibyte nil)
      (insert "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>")
      (svg-print svg_gantt)
      )
    (with-temp-file (format "%s/Gantt_Data.json" og-htmlPath)
      (set-buffer-multibyte nil)
      (insert (format "{\"startDate\":\"%s\",\"endDate\":\"%s\"}"
                      (ts-format "%Y-%m-%d" og-start)
                      (ts-format "%Y-%m-%d" og-end)
                      )
              )
      )
    )
  
  )
;;|--------------------------------------------------------------
;;|Description : draw the today line
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-14-2021 10:14:52
;;|--------------------------------------------------------------
(defun og-draw-today (svg W1 W2 H)
  "draw the today line"
  (let ((X (max W1 (og-xFromDate og-today W1 W2)))
        (Y1 0)
        (Y2 H))
    (svg-line svg X Y1 X Y2 :stroke-width 2 :stroke-color (alist-get 'dayColor og-Cols))
    )
  )
;;|--------------------------------------------------------------
;;|Description : get X as a function of the date
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-10-2021 10:10:40
;;|--------------------------------------------------------------
(defun og-xFromDate (D W1 W2 &optional S E)
  "get X as a function of the date"
  (let* ((s (or S og-printStart))
         (e (or E og-printEnd))
         (total (ts-diff e s))
         (dd    (/ (ts-diff D og-printStart) total)))
    (max W1 (+ W1 (* dd W2)))
    )
  )
;;|--------------------------------------------------------------
;;|Description : create a title block for a gantt
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-59-2020 13:59:06
;;|--------------------------------------------------------------
(defun og-title (svg W H)
  "create a title block for a gantt"
  (svg-rectangle svg 0 0 W H :fill-color (alist-get 'bgTitleBlock og-Cols))
  (let* ((X 10)
         (H (alist-get 'rowH og-Dims))
         (Y1 (* 0.5 H))
         (Y2 (+ Y1  H))
         (Y3 (+ Y2  H))
         (S  (format " - Start: %s" (ts-format (alist-get 'dateFormat og-Text) og-start)))
         (E  (format " - End: %s" (ts-format (alist-get 'dateFormat og-Text) og-end)))
         )
    (svg-text svg og-title :x X :y Y1  :fill "black")
    (svg-text svg S :x X :y Y2 :fill "black")
    (svg-text svg E :x X :y Y3 :fill "black")
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw the gridhead as a function of the scale
;;|NOTE : 
;;|- S = 0 -> Y
;;|- S = 1 -> Y,M
;;|- S = 2 -> Y,M,W
;;|- S = 3 -> M,W,D
;;|Author : jouke hylkema
;;|date   : 23-00-2020 19:00:38
;;|--------------------------------------------------------------
(defun og-gridHead (svg X W H S total)
  "draw the gridhead as a function of the scale"
  (svg-rectangle svg X 0 W H :fill-color (alist-get 'bgGridHead og-Cols))
  (setq Y (alist-get 'rowH og-Dims))
  ;; Years
  (when (< S 3)
    (cl-loop for (X1 p1 p2) = (list X og-printStart (ts-apply :month 1 :day 1 (ts-adjust 'year 1 og-printStart)))
             then (og-draw-gridHead-year svg Y X1 W p1 p2 total (eq S 0))
             when (ts>= p1 og-printEnd)
             return (svg-line svg X Y (+ X W) Y))
    (cl-incf Y (alist-get 'rowH og-Dims)))
  ;; Months
  (when (> S 0)
    (cl-loop for (X1 p1 p2) = (list X og-printStart (ts-apply :day 1 (ts-adjust 'month 1 og-printStart)))
           then (og-draw-gridHead-month svg Y X1 W p1 p2 total (eq S 1))
           when (ts>= p1 og-end)
           return (svg-line svg X Y (+ X W) Y)
           )
    (cl-incf Y (alist-get 'rowH og-Dims)))
  ;; Weeks
  (when (> S 1)
    (cl-loop for (X1 p1 p2) = (list X og-printStart (ts-apply :dow 1 (ts-adjust 'day 7 og-printStart)))
           then (og-draw-gridHead-week svg Y X1 W p1 p2 total (eq S 2))
           when (ts>= p1 og-end)
           return (svg-line svg X Y (+ X W) Y)
           )
    
    (cl-incf Y (alist-get 'rowH og-Dims)))
  ;; Days
  (when (> S 2)
    (cl-loop for (X1 p1 p2) = (list X og-printStart (ts-apply :dom 1 (ts-adjust 'day 1 og-printStart)))
           then (og-draw-gridHead-day svg Y X1 W p1 p2 total (eq S 3))
           when (eq p1 og-end)
           return (svg-line svg X Y (+ X W) Y)
    )
    (cl-incf Y (alist-get 'rowH og-Dims)))
  
 )
;;|--------------------------------------------------------------
;;|Description : recursively draw a gridHead year
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-07-2020 09:07:43
;;|--------------------------------------------------------------
(defun og-draw-gridHead-year (svg Y X1 W p1 p2 total long)
  "draw a gridHead year"
  
  (let* ((name (ts-Y p1))
         (fs   (alist-get 'fontSize og-Text))
         (p2   (if (ts< og-printEnd p2) og-printEnd p2))
         (dd   (/ (ts-diff p2 p1) total))
         (X2   (+ X1 (* dd W)))
         (Xt   (* 0.5 (+ X1 X2)))
         (Y1   (- Y (alist-get 'rowH og-Dims)))
         (Y2   (if long (* (alist-get 'rowH og-Dims)
                           (+ (length og-project) (alist-get 'headRows og-Dims)))
                 Y))
         (Yt   (+ Y1 (* 0.6 (alist-get 'rowH og-Dims))))
         )
    ;; (svg-line svg X1 Y1 X1 Y2)
    (svg-line svg X1 Y1 X1 Y)
    (svg-text svg (format "%s" name) :x Xt :y Yt
              :fill "black"
              :font-size fs
              :text-anchor "middle"
              :stroke-width (alist-get 'sw og-Text))
    (list X2 p2 (ts-adjust 'year 1 p2))
    )
  )
;;|--------------------------------------------------------------
;;|Description : recursively draw a gridHead month
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-07-2020 09:07:43
;;|--------------------------------------------------------------
(defun og-draw-gridHead-month (svg Y X1 W p1 p2 total long)
  "draw a gridHead month"
  (let* ((name (ts-moy p1))
         (fs   (* 0.8 (alist-get 'fontSize og-Text)))
         (p2   (if (ts< og-end p2) og-end p2))
         (dd   (/ (ts-diff p2 p1) total))
         (X2   (+ X1 (* dd W)))
         (Xt   (* 0.5 (+ X1 X2)))
         (Y1   (- Y (alist-get 'rowH og-Dims)))
         (Y2   (if long (* (alist-get 'rowH og-Dims)
                           (+ (length og-project) (alist-get 'headRows og-Dims)))
                 Y))
         (Yt   (+ Y1 (* 0.6 (alist-get 'rowH og-Dims))))
         )
    ;; (svg-line svg X1 Y1 X1 Y2)
    (svg-line svg X1 Y1 X1 Y)
    (svg-text svg (format "%s" name) :x Xt :y Yt
              :fill "black"
              :font-size fs :text-anchor "middle"
              :stroke-width (alist-get 'sw og-Text))
    (list X2 p2 (ts-adjust 'month 1 p2))
    )
  )
;;|--------------------------------------------------------------
;;|Description : recursively draw a gridHead week
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-07-2020 09:07:43
;;|--------------------------------------------------------------
(defun og-draw-gridHead-week (svg Y X1 W p1 p2 total long)
  "draw a gridHead week"
  (let* ((name (ts-woy p1))
         (fs   (* 0.6 (alist-get 'fontSize og-Text)))
         (p2   (if (ts< og-end p2) og-end p2))
         (dd   (/ (ts-diff p2 p1) total))
         (X2   (+ X1 (* dd W)))
         (Xt   (* 0.5 (+ X1 X2)))
         (Y1   (- Y (alist-get 'rowH og-Dims)))
         (Y2   (if long (* (alist-get 'rowH og-Dims)
                           (+ (length og-project) (alist-get 'headRows og-Dims)))
                 Y))
         (Yt   (+ Y1 (* 0.5 (alist-get 'rowH og-Dims))))
         )
    ;; (svg-line svg X1 Y1 X1 Y2)
    (svg-line svg X1 Y1 X1 Y)
    (svg-text svg (format "%s" name) :x Xt :y Yt
              :fill "black"
              :font-size fs :text-anchor "middle"
              :stroke-width (alist-get 'sw og-Text))
    (list X2 p2 (ts-adjust 'day 7 p2))
    )
  )
;;|--------------------------------------------------------------
;;|Description : recursively draw a gridHead day
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-07-2020 09:07:43
;;|--------------------------------------------------------------
(defun og-draw-gridHead-day (svg Y X1 W p1 p2 total long)
  "draw a gridHead week"
  (let* ((name (ts-dom p1))
         (fs   (* 0.6 (alist-get 'fontSize og-Text)))
         (p2   (if (ts< og-end p2) og-end p2))
         (dd   (/ (ts-diff p2 p1) total))
         (X2   (+ X1 (* dd W)))
         (Xt   (* 0.5 (+ X1 X2)))
         (Y1   (- Y (alist-get 'rowH og-Dims)))
         (Y2   (if long (* (alist-get 'rowH og-Dims)
                           (+ (length og-project) (alist-get 'headRows og-Dims)))
                 Y))
         (Yt   (+ Y1 (* 0.5 (alist-get 'rowH og-Dims))))
         (we  (or (eq (ts-dow p1) 0) (eq (ts-dow p1) 6)))
         (col (if we (alist-get 'weekend og-Cols) (alist-get 'week og-Cols)))
         )
    ;; (svg-line svg X1 Y1 X1 Y2)
    (svg-line svg X1 Y1 X1 Y)
    (if we (svg-rectangle svg X1 Y1 (- X2 X1) (- Y2 Y1)
                          :fill col
                          :opacity "0.3"
                          )
      )
    (svg-text svg (format "%s" name) :x Xt :y Yt
              :fill col
              :stroke col
              :font-size fs :text-anchor "middle"
              :stroke-width (alist-get 'sw og-Text))
    (list X2 p2 (ts-adjust 'day 1 p2))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw a task
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-57-2020 10:57:52
;;|--------------------------------------------------------------
(defun og-draw-task (svg task row W1 W2)
  "Draw a task"
  (if (and
       (ts> (myTask-End task) og-printStart)
       (ts< (myTask-Start task) og-printEnd)
       )
      (progn
      (og-task-left  svg task row W1 (alist-get 'headRows og-Dims))
      (og-task-right svg task row W1 W2 (alist-get 'headRows og-Dims))
      )
    (setf (myTask-Hidden task) t)
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the left part of a task
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-16-2023 11:16:44
;;|--------------------------------------------------------------
(defun og-task-left (svg task row W1 deltaY)
  "Draw the left part of a task"
  (interactive "P")
  (let* ((col (cond ((eq 0 (myTask-Level task)) (alist-get 'bgTaskLevel1 og-Cols))
                    ((eq 0 (% row 2))           (alist-get 'bgTaskEven og-Cols))
                    (t                          (alist-get 'bgTaskOdd og-Cols))))
         (H   (alist-get 'rowH og-Dims))
         (H2  (* 0.5 H))
         (Y1  (* (+ deltaY row) H))
         (Y2  (+ Y1 H))
         (Y   (* 0.5 (+ Y1 Y2)))
         (fs  (* 0.6 (alist-get 'fontSize og-Text)))
         (X1  (* (myTask-Done task) W1))
         (id0 (format "ID0_%s" (myTask-Id task)))
         (id1 (format "ID1_%s" (myTask-Id task)))
         )
    ;; (message "Draw row %s %s,%s->%s,%s col=%s" row 0 Y W1 Y col)
    (svg-line svg 0 Y W1 Y
              :stroke-color col
              :stroke-width H
              :opacity "0.4"
              )
    (svg-line svg 0 Y X1 Y
              :stroke-color (alist-get 'taskDone og-Cols)
              :stroke-width H
              :opacity "0.3"
              )
    (svg-line svg W1 Y1 W1 Y2)
    (svg-text svg (format "%s" (myTask-Id task) )
              :x 2
              :y Y
              :fill "black"
              :font-size fs
              :id id0
              )
    (svg-text svg (myTask-Description task)
              :x (alist-get 'taskWidth og-Dims)
              :y Y
              :fill "black"
              :font-size fs
              :id id1
              :stroke-width (alist-get 'sw og-Text)
              )
    (og-task-info task id1 "long")
    )
  )
;;|--------------------------------------------------------------
;;|Description : Draw the right part of the task
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-04-2023 12:04:36
;;|--------------------------------------------------------------
(defun og-task-right (svg task row X0 W2 deltaY)
  "Draw the right part of the task"
  (interactive "P")
  (let* ((col (cond ((eq 0 (myTask-Level task)) (alist-get 'bgTaskLevel1 og-Cols))
                    ((eq 0 (% row 2))           (alist-get 'bgTaskEven og-Cols))
                    (t                          (alist-get 'bgTaskOdd og-Cols))))
         (H   (alist-get 'rowH og-Dims))
         (H2  (* 0.5 H))
         (m   2)
         (Y1  (* (+ deltaY row) H))
         (Y2  (+ Y1 H))
         (Y   (* 0.5 (+ Y1 Y2)))
         (fs  (* 0.6 (alist-get 'fontSize og-Text)))
         (X2  (og-xFromDate (myTask-Start task) X0 W2))
         (X4  (og-xFromDate (myTask-End task) X0 W2))
         (X3  (+ X2 (* (myTask-Done task) (- X4 X2))))
         (X5  (+ X0 W2))
         (X6  (og-xFromDate (og-calc-end-from-start task) X0 W2))
         (id1 (format "ID1_%s" (myTask-Id task)))
         (id2 (format "ID2_%s" (myTask-Id task)))
         )
    
    (setf (myTask-X1 task) X2)
    (setf (myTask-X2 task) X4)
    (setf (myTask-Y  task) Y)

    (svg-line svg X0 Y X5 Y
              :stroke-color col
              :stroke-width H
              :opacity "0.4"
              )
    
    (cond
     ;; Key points centered around X2
     ((string= (myTask-Type task) "KP")
      (let* ((x1 (- X2 H2))
             (x2 X2)
             (x3 (+ X2 H2))
             (y1 Y1)
             (y2 (+ Y1 H2))
             (y3 Y2)
             (col (if (>= (myTask-Done task) 1) (alist-get 'bgKPdone og-Cols) (alist-get 'bgKP og-Cols)))
             )
        (svg-polygon svg `((,x1 . ,y2)
                           (,x2 . ,y1)
                           (,x3 . ,y2)
                           (,x2 . ,y3))
                     :fill-color col
                     :id id2
                     )
        )
      )
     ;; Regular tasks
     ((> (myTask-Level task) 0)
      (if (myTask-Kids task)
          (og-draw-hat X2 X4 Y (- H m) 5 (alist-get 'bgTaskP og-Cols) id2)
        (svg-line svg X2 Y X4 Y
                  :stroke-color (alist-get 'bgTask og-Cols)
                  :stroke-width (- H m)
                  :id id2
                  )
        )
      
      (svg-line svg X2 Y X3 Y :stroke-color (alist-get 'taskDone og-Cols) :stroke-width 5)
      (svg-line svg X2 (- Y 5) X6 (- Y 5) :stroke-color (alist-get 'taskDuration og-Cols) :stroke-width 5)
      )
     ;; Task level 0, a hat like polygon
     ((eq (myTask-Level task) 0)
      (og-draw-hat X2 X4 Y H (/ H 2) (alist-get 'fillTaskLevel1 og-Cols) id2)
      )
     )
    (og-task-info task id2 "short")
    )
  )
;;|--------------------------------------------------------------
;;|Description : Manage task popup info and stuff
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-14-2021 17:14:54
;;|--------------------------------------------------------------
(defun og-task-info (task id type)
  "Manage task popup info and stuff"
  (let ((target (dom-by-id svg id))
        (popup (cond ((string= "short" type)
                      (format "dates: %s->%s"
                              (ts-format "%d/%m/%Y" (myTask-Start task))
                              (ts-format "%d/%m/%Y" (myTask-End task)))
                      )
                     ((string= "long" type)
                      (format "dates: %s->%s\n<foreignObject x='0' y='0' width='200' height='400'>%s\n%s\n%s</foreignObject>"
                              (ts-format "%d/%m/%Y" (myTask-Start task))
                              (ts-format "%d/%m/%Y" (myTask-End task))
                              (format "resp:  %s" (myTask-Resp task))
                              (format "avec: %s" (string-join (myTask-Avec task) ","))
                              (format "notes: %s" (org-export-string-as (myTask-Content task) 'html t))
                              (when (myTask-Schema task)
                                (add-to-list 'og-schemas `(,(myTask-Schema task) ,(myTask-Id task))))
                              )
                      )   
                     )
               )
        )
    (dom-append-child target (format "<title>%s</title>" popup))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Insert an image in the popup if SCHEMA is present
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-50-2023 10:50:44
;;|--------------------------------------------------------------
(defun og-schema (s svg &optional external)
  "Insert schemas"
  (let* ((path (format "http://%s" (nth 0 s)))
         (id1 (format "schema_%s" (nth 1 s)))
         (id2 (format "ID0_%s" (nth 1 s)))
         )
    (dom-set-attribute
     (dom-by-id svg id2)
     "onclick"
     (format "window.open('%s')" path)
     )
    (dom-set-attribute
     (dom-by-id svg id2)
     "text-decoration"
     "underline"
     )
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw a hat
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-20-2021 20:20:09
;;|--------------------------------------------------------------
(defun og-draw-hat (Xs Xe Y H h col id)
  "draw a hat"
  (let* ((x1 Xs)
         (x2 (+ Xs h))
         (x3 (- Xe h))
         (x4 Xe)
         (y1 (- Y (/ H 2)))
         (y2 (+ y1 (- H h)))
         (y3 (+ y1 H))
         )
    (svg-polygon svg `((,x1 . ,y1)
                       (,x4 . ,y1)
                       (,x4 . ,y3)
                       (,x3 . ,y2)
                       (,x2 . ,y2)
                       (,x1 . ,y3))
                 :fill-color col
                 :id id)
    )
  )
;;|--------------------------------------------------------------
;;|Description : draw the links between tasks
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-29-2020 14:29:50
;;|--------------------------------------------------------------
(defun og-draw-links (svg task)
  "draw the links between tasks"
  (unless (eq 0 (length (myTask-After task)))
    (cl-loop for A in (myTask-After task) do (og-draw-link svg A task)))
  )
;;|--------------------------------------------------------------
;;|Description : draw the actual link between a and b
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 20-33-2020 14:33:28
;;|--------------------------------------------------------------
(defun og-draw-link (svg A B)
  "draw the actual link between a and b"
  ;; (message "A: %s" (type-of A))
  ;; (message "B: %s" (type-of B))
  (unless (or (eq (type-of A) 'string) (eq (type-of B) 'string) (myTask-Hidden A) (myTask-Hidden B))
    (let* ((X1 (myTask-X2 A))
           (X2 (myTask-X1 B))
           (Y1 (myTask-Y A))
           (Y2 (myTask-Y B))
           (d 5)
           (c (alist-get 'stLink og-Cols))
           )
      (svg-polyline svg `((,X1 . ,Y1) (,X1 . ,Y2) (,X2 . ,Y2)) :stroke-width 2 :stroke-color c)
      (svg-polygon svg `((,X2 . ,Y2) (,X2 . ,(+ Y2 d)) (,(+ X2 d) . ,Y2) (,X2 . ,(- Y2 d))) :fill-color c)
      )
    )
  )
;;|--------------------------------------------------------------
;;|Description : save the gantt to pdf and png
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-40-2021 20:40:23
;;|--------------------------------------------------------------
(defun og-save ()
  "save the gantt to pdf and png"
  (shell-command (format "inkscape --export-type=pdf %s.svg" og-svgPath))
  (shell-command (format "inkscape --export-type=png %s.svg" og-svgPath))
  )
;;|--------------------------------------------------------------
;;|Description : Make the report bit
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 17-37-2022 10:37:04
;;|--------------------------------------------------------------
(defun og-report ()
  "Make the report bit"
  (interactive)
  (let* ((id (org-entry-get nil "ID" t))
         ;; (R ())
         )
    (org-id-goto (format "resp_%s" id))
    (myOrg-clear-subtree)
    (insert "#+ATTR_LATEX: :environment longtblr\n")
    (insert "#+ATTR_LATEX: :align width=\\linewidth,")
    (insert "colspec={lllllX},")
    (insert "vlines=1pt,hline{1,2,Z}=1pt\n")
    (insert "|Responsable|Avec|Tache|Début|Fin|Description|\n")
    (cl-loop for task in (cl-sort (cl-subseq og-project 1) #'string-lessp :key #'myTask-Resp) do
             (unless (string= (myTask-Type task) "KP")
               (insert (format "|%s|%s|%s|%s|%s|%s|\n"
                               (myTask-Resp task)
                               (string-join (myTask-Avec task) ",")
                               (myTask-Id task)
                               (ts-format (alist-get 'dateFormat og-Text) (myTask-Start task))
                               (ts-format (alist-get 'dateFormat og-Text)(myTask-End task))
                               (og-complete-description task)
                               )
                       )
               )
             )
    (newline 2)
    )
  )
;;|--------------------------------------------------------------
;;|Description : combine content and description
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 23-20-2022 14:20:21
;;|--------------------------------------------------------------
(defun og-complete-description (task)
  "combine content and description"
  (interactive)
  (let ((out (format "%s : %s"
                     (myTask-Description task)
                     (s-replace "\n" ". " (myTask-Content task)))))
    (if (> (length out) 100) (format "%s ..." (substring out 0 80)) out)
    )
  )
(provide 'org-gantt-draw)
;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
