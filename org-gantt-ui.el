;;|--------------------------------------------------------------
;;|Description : get a list of all task ids
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 24-35-2021 11:35:48
;;|--------------------------------------------------------------
(defun og-getTasks ()
  "get a list of all task ids"
  (interactive)
  (save-excursion
    ;; Goto the top level heading
    (cl-loop until (string= (org-entry-get (point) "TASK_ID") "Title") do (outline-up-heading 1))
    (remove nil
            (org-map-entries
             (lambda ()
               (format "%s - %s"
                       (org-entry-get (point) "TASK_ID")
                       (org-entry-get (point) "ITEM")
                       )
               )
             nil 'tree)
            )
    )
  )
;;|--------------------------------------------------------------
;;|Description : filter tasks
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 02-47-2021 13:47:54
;;|--------------------------------------------------------------
(defun og-filter (Tasks Filter &optional default)
  "filter tasks"
  (let* ((tmp (cl-loop for tsk in Tasks
                       when (string-match Filter tsk)
                       collect tsk))
         (out (if tmp tmp (if default default nil))))
    out
    )
  )
;;|--------------------------------------------------------------
;;|Description : Insert a task
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 24-45-2021 10:45:25
;;|--------------------------------------------------------------
(defun og-insert-task ()
  "Insert a task"
  (interactive)
  ;; (message "%s" (og-getTasks))
  (let* ((Tasks (append '("-") (og-getTasks)))
         (Resps (split-string-and-unquote (org-entry-get (point) "RESP_ALL" t) " "))
         (Title (read-string "Title: "))
         (Type (ido-completing-read "Type: " (list "-" "KP")))
         (oldId (split-string (org-entry-get (point) "TASK_ID") "\\."))
         (ID (read-string "ID: "
                          (format "%s.%s"
                                  (string-join (butlast oldId) ".")
                                  (+ 1 (string-to-number (car (last oldId)))))
                          )
             )
         (After (helm-comp-read "After (c-spc to tag): " Tasks :marked-candidates t))
         (Before (helm-comp-read "Before (c-spc to tag): " Tasks :marked-candidates t))
         (Start (if (y-or-n-p "Start date ?") (org-read-date nil nil nil "Start: ") nil))
         (End (if (y-or-n-p "End date ?") (org-read-date nil nil nil "End: ") nil))
         (Duration (if (string= "KP" Type) "1d" (read-string "Duration: ")))
         (Resp (ido-completing-read "Responsable: " Resps))
         (Avec (helm-comp-read "Avec (c-spc to tag): " (append '("-") Resps) :marked-candidates t))
         (Done "0.0")
         (Obj (if (string= "KP" Type) (read-string "Objective: ")))
         (Deliverable (read-string "Deliverable: "))
         (Schema (read-file-name "Schema: "))
         )
    (org-insert-heading-after-current)
    (org-edit-headline (format "%s:%s" ID Title))
    (org-entry-put (point) "TASK_ID" ID)
    (unless (string= "-" (car Before))
      (org-entry-put (point) "BEFORE" (mapconcat (lambda (x) (nth 0 (split-string x " - "))) Before ",")))
    (unless (string= "-" (car After))
      (org-entry-put (point) "AFTER"  (mapconcat (lambda (x) (nth 0 (split-string x " - "))) After ",")))
    (unless (string= "-" Type)    (org-entry-put (point) "TYPE" Type))
    (if Start    (org-entry-put (point) "RSTART"  Start    ))
    (if End      (org-entry-put (point) "REND"    End      ))
    (if Duration (org-entry-put (point) "EFFORT" Duration ))
    (unless (string= "-" Resp)  (org-entry-put (point) "RESP"   Resp     ))
    (unless (string= "-" (car Avec))  (org-entry-put (point) "AVEC" (string-join Avec ",") ))
    (org-entry-put (point) "DONE" Done)
    (if (string= "KP" Type) (org-entry-put (point) "OBJECTIVE" Obj))
    (org-entry-put (point) "DELIVERABLE" Deliverable)
    (unless (string= "/" Schema) (org-entry-put (point) "SCHEMA" Schema))
    )
  )
;;|--------------------------------------------------------------
;;|Description : setAfter/Before
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 13-31-2022 17:31:03
;;|--------------------------------------------------------------
(defun og-setAfter ()
  "set After"
  (interactive)
  (let ((old (org-entry-get (point) "AFTER"))
        (new (nth 0 (split-string (ido-completing-read "After: " (append '("-") (og-getTasks)))))))
    (org-entry-put (point) "AFTER" (string-join (list old new) ","))
    )
  )
(defun og-setBefore ()
  "set Before"
  (interactive)
  (let ((old (org-entry-get (point) "BEFORE"))
        (new (nth 0 (split-string (ido-completing-read "Before: " (append '("-") (og-getTasks)))))))
    (org-entry-put (point) "BEFORE"(string-join (list old new) ","))
    )
  )
;;|--------------------------------------------------------------
;;|Description : Add a map to the image
;;|NOTE : You have to execute this while in the buffer that contains the gantt image
;;|-
;;|Author : jouke hylkema
;;|date   : 30-14-2021 16:14:39
;;|--------------------------------------------------------------
(defun og-add-map ()
  "Add a map to the image"
  (interactive)
  (message "og-map = %s" og-map)
  (let* ((map og-map)
         (imageBuffer (get-buffer-create "*org-gantt-image*"))
          )
    (switch-to-buffer-other-window imageBuffer)
    (erase-buffer)
    (find-file "/home/hylkema/Documents/ESA/Waterhammer/Propal/Images/Gantt.svg")
    (image-transform-fit-to-width)
    (message "%s" (image--get-image))
    (image--set-property
     (image--get-image)
     :map
     '(((rect . ((0 . 0) . (500 . 500)))
        "id1"
        (pointer hand help-echo "yes I found it")
        )
       )
     )
    )
  (message "%s" (image--get-image))
  )

;;|--------------------------------------------------------------
;;|Description : Find the first buffer that runs image-mode
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-31-2021 16:31:16
;;|--------------------------------------------------------------
(defun og-select-buffer (mode)
  "Find the first buffer that runs image-mode"
  (let ((b (cl-loop for b being the elements of (buffer-list)
                    when (string= (with-current-buffer b major-mode) mode)
                    collect (buffer-name b)
                    )))
    (ido-completing-read "Pick buffer: " b)
    )
  )

;;|--------------------------------------------------------------
;;|Description : Debug
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 30-46-2021 17:46:57
;;|--------------------------------------------------------------
(defun og-debug ()
  "Debug"
  (interactive)
  (message "%s" (car (last (og-filter (og-getTasks) "KP[[:digit:]]+[\.[:digit:]]* - "))))
  
  
  )
;;|--------------------------------------------------------------
;;|Description : export report to pdf
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 17-12-2022 10:12:40
;;|--------------------------------------------------------------
(defun og-export-report (id)
  "export report to pdf"
  (interactive)
  (org-id-goto id)
  (org-latex-export-to-pdf nil 's)
  )
(provide 'org-gantt-ui)
;;|--------------------------------------------------------------
;;|Description : Insert a complete planning bloc
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 17-46-2022 15:46:04
;;|--------------------------------------------------------------
(defun og-new-planning ()
  "Insert a complete planning bloc"
  (interactive)
  (let* ((id (org-id-new))
         (title (read-string "title: "))
         (resp "Jouke Cedric JC Seb Christophe Juliette")
         (start (org-read-date nil nil nil "start date:"))
         (out (expand-file-name (read-directory-name "Output directory: " "~/Documents")))
         (outHtml (expand-file-name (read-directory-name "Output directory: " "~/webSites/Planning")))
         (aer (org-entry-get (point) "AER" t))
         (lang "french")
         )
    
    (mkdir out t)
    (org-insert-heading nil t)
    (org-do-demote)
    (insert (format "Planning: %s" title))
    (org-set-property "ID" id)
    (org-set-property "Task_ID" "Title")
    (org-set-property "COLUMNS" "%ITEM(item) %TASK_ID(task_id) %EFFORT(eff) %BEFORE(before) %AFTER(after) %RSTART(start) %REND(end) %TYPE(type) %RESP(resp) %OBJECTIVE(objective)")
    (org-set-property "RESP_ALL" (cl-loop for r in (s-split " " resp) concat (s-concat "\"" r "\" ")))
    (org-set-property "RSTART"   start)
    (org-set-property "PRINT_START"   start)
    (org-set-property "SCALE"   "2")
    (org-set-property "SVG_PATH" (format "%sGantt" out))
    (org-set-property "HTML_PATH" (format "%s" outHtml))
    (org-end-of-meta-data t)
    (forward-line)
    (insert "Use og-insert-task to add tasks\nUse og-set(After,Before) to add constraints\n\n")
    
    (org-insert-heading-after-current)
    (insert (format "Rapport planning: %s" title))
    (org-set-property "ID" (format "rap_%s" id))
    (org-set-property "EXPORT_LATEX_CLASS_OPTIONS" (format "[type=planning,aer=%s,lang=%s,title=%s]"
                                                           aer lang (s-replace " " "~" title)))
    (org-set-property "EXPORT_FILE_NAME" (format "%sReport.pdf" out))
    (org-end-of-meta-data t)
    (forward-line)
    (insert (format "#+ATTR_LATEX: :width \\linewidth \n [[file:%sGantt.pdf]]" out))

    (org-insert-heading-after-current)
    (org-demote-subtree)
    (insert (format "[[elisp:(og-export-report 'rap_%s)][export to pdf]]" id))

    (org-insert-heading-after-current)
    (org-set-property "ID" (format "tache_%s" id))
    (insert "Taches:")
                      
    (org-insert-heading-after-current)
    (org-set-property "ID" (format "resp_%s" id))
    (insert "Responsables:")
                      
    )
  )
(provide 'org-gantt-ui)
;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:

