var mySVG    = "";
var myHead   = "";
var oldStart = Date.now();
var oldEnd   = Date.now();

var oldW  = ""
var oldH1 = ""
var oldH2 = ""

var newW = ""
var newS = "0"
var Projects = {
    "Waterhammer":"Waterhammer",
    "lunarIslands":"lunarIslands"
}

var Data

// --------------------------------------------------------------
// Description : initialise values
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 24-23-2023 15:23:26
// --------------------------------------------------------------
window.onload = function(){
    console.log("window loaded");
    for (p in Projects) {
        console.log(p);
        opt = document.createElement("option");
        opt.text = p;
        opt.value  = Projects[p];
        document.getElementById("Projects").add(opt,null);
    }
    document.getElementById("ganttWindow").setAttribute("onload", "setSvgSource('mySVG')");
    document.getElementById("headWindow").setAttribute("onload", "setSvgSource('myHead')");
}
// --------------------------------------------------------------
// Description : set svg variable
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 30-29-2024 15:29:41
// --------------------------------------------------------------
function setSvgSource(name) {
    switch (name) {
    case "mySVG":
        mySVG     = document.getElementById("ganttWindow").getSVGDocument().getElementById("svgGanttId");
        oldSvgVB  = mySVG.getAttribute("viewBox");
        oldW      = +oldSvgVB.split(" ")[2];
        newW      = oldW;
        oldH1     = +oldSvgVB.split(" ")[3];
        resetDates();
        console.log("mySVG loaded");
        drawToday();
        break;
    case "myHead":
        myHead    = document.getElementById("headWindow").getSVGDocument().getElementById("svgHeadId");
        oldHeadVB = myHead.getAttribute("viewBox");
        oldH2     = +oldHeadVB.split(" ")[3];
        console.log("myHead loaded");
        break;
    }
}
// --------------------------------------------------------------
// Description : load a project
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 30-20-2024 14:20:11
// --------------------------------------------------------------
async function loadProject(path) {

    console.log("load project",path);
    const response = await fetch(path+"/Gantt_Data.json")
    Data = await response.json()

    document.getElementById("titleWindow").setAttribute("data",path+"/Gantt_title.svg")
    document.getElementById("headWindow").setAttribute("data",path+"/Gantt_head.svg")
    document.getElementById("taskWindow").setAttribute("data",path+"/Gantt_tasks.svg")
    document.getElementById("ganttWindow").setAttribute("data",path+"/Gantt_gantt.svg")

    oldStart  = new Date(Data["startDate"]);
    oldEnd    = new Date(Data["endDate"]);
    newStart  = oldStart;
    newEnd    = oldEnd;
    
    console.log("loaded");
}
// --------------------------------------------------------------
// Description : set start date
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 20-07-2023 18:07:32
// --------------------------------------------------------------
function setStart() {
    var start = new Date(document.getElementById("startDate").value.split("-"))
    if (start < oldStart) {
        alert("Je ne peux pas commencer avant le début");
    } else {
        var L = oldEnd-oldStart;
        var l = start-oldStart;
        newW = oldW*(L-l)/L;
        newS = oldW*(l/L);
        mySVG.setAttribute("viewBox" ,newS+" 0 "+newW+" "+oldH1);
        myHead.setAttribute("viewBox",newS+" 0 "+newW+" "+oldH2);
    }
}
// --------------------------------------------------------------
// Description : set end date
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 20-57-2023 18:57:25
// --------------------------------------------------------------
function setEnd() {
    var end = new Date(document.getElementById("endDate").value.split("-"))
    if (end > oldEnd) {
        alert("Je ne peux pas arreter après la fin ("+
              oldEnd.toLocaleDateString('fr', { weekday:"long", year:"numeric", month:"short", day:"numeric"}) +")");
    } else {
        var L = oldEnd-oldStart;
        var l = oldEnd-end;
        newW = (oldW-newS)*(L-l)/L;
        mySVG.setAttribute("viewBox" ,newS+" 0 "+newW+" "+oldH1);
        myHead.setAttribute("viewBox",newS+" 0 "+newW+" "+oldH2);
    }
}
// --------------------------------------------------------------
// Description : reset dates
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 20-57-2023 18:57:47
// --------------------------------------------------------------
function resetDates() {
    mySVG.setAttribute("viewBox",oldSvgVB);
    myHead.setAttribute("viewBox",oldHeadVB);
    document.getElementById("startDate").value=Data["startDate"]
    document.getElementById("endDate").value=Data["endDate"]
    newS = "0"
}
// --------------------------------------------------------------
// Description : Draw todays line
// NOTE :
// -
// Author : Jouke Hijlkema
// date   : 30-34-2024 18:34:02
// --------------------------------------------------------------
function drawToday() {
    var today = new Date()
    var L = oldEnd-oldStart;
    var l = oldEnd-today;
    let X = (oldW-newS)*(L-l)/L;
    let Y2 = oldH1;
    var newLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    newLine.setAttribute("stroke", "purple");
    newLine.setAttribute("x1",X);
    newLine.setAttribute("y1",0);
    newLine.setAttribute("x2",X);
    newLine.setAttribute("y2",Y2);
    mySVG.appendChild(newLine);
}
