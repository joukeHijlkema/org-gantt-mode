;;|--------------------------------------------------------------
;;|Description : Parse the heading into the project
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-23-2021 10:23:35
;;|--------------------------------------------------------------
(defun og-parse (&optional edna)
  "Parse the heading into the project"
  (interactive)
  (message "Start og-parse")
  (save-excursion
    (cl-loop until (string= (org-entry-get (point) "TASK_ID") "Title") do (outline-up-heading 1))
    (setq og-levelOffset (org-current-level))
    (setq og-title (org-entry-get (point) "ITEM"))
    (setq og-start (ts-parse (org-entry-get (point) "RSTART")))
    (setq og-end og-start)
    (setq og-svgPath (org-entry-get (point) "SVG_PATH"))
    (setq og-htmlPath (org-entry-get (point) "HTML_PATH"))
    (setq og-today (if (org-entry-get (point) "TODAY") (ts-parse (org-entry-get (point) "TODAY")) (ts-now)))
    (setq og-bg-color (org-entry-get (point) "BG_COLOR"))
    (setq og-index 0)
    (setq og-project (org-map-entries 'og-parse-task nil 'tree))
    (setq og-printStart (if (org-entry-get (point) "PRINT_START")
                            (ts-parse (org-entry-get (point) "PRINT_START"))
                          og-start))
    (setq og-printEnd (if (org-entry-get (point) "PRINT_END")
                          (ts-parse (org-entry-get (point) "PRINT_END"))
                        nil))
    )
  (message "og-parse done")
  )
;;|--------------------------------------------------------------
;;|Description : Parse an item into a task object
;;|NOTE : point is at the beginning of the item
;;|-
;;|Author : jouke hylkema
;;|date   : 25-33-2021 10:33:10
;;|--------------------------------------------------------------
(defun og-parse-task ()
  "Parse an item into a task object"
  (interactive)
  ;; (message "task %s RSTART = %s REND = %s"
           ;; (org-entry-get (point) "TASK_ID")
           ;; (org-entry-get (point) "RSTART")
  ;; (org-entry-get (point) "REND"))
  (setq og-index (+ 1 og-index))
  (let* ((Level (- (org-current-level) og-levelOffset))
         (Duration (cond ((string= (org-entry-get (point) "TYPE") "KP") "1d")
                         (t (org-entry-get (point) "EFFORT"))))
         )
     (when (org-entry-get (point) "RESP")
      (add-to-list 'og-resources (org-entry-get (point) "RESP"))
      )
   (make-myTask
     :Id           (org-entry-get (point) "TASK_ID")
     :Description  (org-entry-get (point) "ITEM")
     :Type         (org-entry-get (point) "TYPE")
     :Before       (split-string (or (org-entry-get (point) "BEFORE") "") ",")
     :After        (split-string (or (org-entry-get (point) "AFTER")  "") ",")
     :RStart       (og-time-parse (org-entry-get (point) "RSTART"))
     :REnd         (og-time-parse (org-entry-get (point) "REND"))
     :Start        og-start
     :End          og-start
     :Level        Level
     :Kids         (list)
     :Duration     Duration
     :Objective    (org-entry-get (point) "OBJECTIVE")
     :Content      (og-get-content)
     :Resp         (org-entry-get (point) "RESP")
     :Avec         (split-string (or (org-entry-get (point) "AVEC") "") ",")
     :Done         (string-to-number (format "%s" (org-entry-get (point) "DONE")))
     :Schema       (org-entry-get (point) "SCHEMA")
     :Hidden       nil
     :Index        (format "%s" og-index)
     )
    )
  )
;;|--------------------------------------------------------------
;;|Description : my time parser to deal with nil and ""
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 26-21-2021 11:21:00
;;|--------------------------------------------------------------
(defun og-time-parse (T)
  "my time parser to deal with nil and "
  (interactive)
  ;; (message "Time = -%s- (%s)" T (string= T "") (org-timestamp-from-string T))
  (cond ((string= T "") nil)
        (T (ts-parse T))
        (t nil))
  )
;;|--------------------------------------------------------------
;;|Description : get a Task from an ID
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-24-2020 16:24:23
;;|--------------------------------------------------------------
(defun og-get-task-from-id (id)
  "get a Task from an ID"
  (interactive "P")
  ;; (message "looking for %s" id)
  (car (seq-filter (lambda (B) (string= id (myTask-Id B))) og-project)))
;;|--------------------------------------------------------------
;;|Description : Replace Id with task
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-42-2021 11:42:53
;;|--------------------------------------------------------------
(defun og-fix-BA ()
  "Replace Id with task"
  (message "Start og-fix-BA")
  (cl-loop for task in og-project do
        (let ((After (list))
              (AOffset (list))
              (Before (list))
              (BOffset (list)))
          (cl-loop for A in (myTask-After task) do
                (let* ((target (og-get-task-from-id (car (split-string A "+"))))
                       (offset (cadr (split-string A "+"))))
                  (if target (push target After))
                  (if offset (push offset AOffset) (push "0" AOffset))
                  )
                )
          (cl-loop for B in (myTask-Before task) do
                   (let* ((target (og-get-task-from-id (car (split-string B "+"))))
                          (offset (cdr (split-string B "+"))))
                     (if target (push target Before))
                     (if offset (push offset BOffset))
                     )
                   )
          (setf (myTask-After task) After)
          (setf (myTask-Before task) Before)
          (setf (myTask-startOffset task) AOffset)
          (setf (myTask-endOffset task) BOffset)
          )
        )
  (message "Done og-fix-BA")
  )
;;|--------------------------------------------------------------
;;|Description : fix the hierarchy between tasks
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-05-2020 17:05:21
;;|--------------------------------------------------------------
(defun og-fix-hierarchy ()
  "fix the hierarchy between tasks"
  (message "Start og-fix-hierarchy")
  (cl-loop for task in og-project do
           (when (> (myTask-Level task) 1)
             ;; (message "dbg: %s" (myTask-Id task))
             (let* ((parentId (string-join (seq-subseq (split-string (myTask-Id task) "\\.") 0 -1) "."))
                    (parent (og-get-task-from-id parentId))
                    (kids (myTask-Kids parent)))
               (push task kids)
               (setf (myTask-Parent task) parent)
               (push task (myTask-Kids parent))      
               )
             )
           )
  (message "Done og-fix-hierarchy")
  )
;;|--------------------------------------------------------------
;;|Description : Print the project
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 03-36-2020 18:36:05
;;|--------------------------------------------------------------
(defun og-print-project ()
  "Print the project"
  (interactive "P")
  (message "=======================")
  (message "Project %s" og-title) 
  (message "start: %s" (ts-format (alist-get 'dateFormat og-Text) og-start))
  (message "end  : %s" (ts-format (alist-get 'dateFormat og-Text) og-end))
  (message "=======================")
  (cl-loop for task in og-project do (og-print-task task))
  )
;;|--------------------------------------------------------------
;;|Description : Print a task
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 18-14-2020 15:14:55
;;|--------------------------------------------------------------
(defun og-print-task (task)
  "Print a task"
  (interactive "P")
  (message "=== Task %s ===" (myTask-Id task))
  (message "| Type        : %s" (myTask-Type task))
  (message "| Level       : %s" (myTask-Level task))
  (message "| Description : %s" (myTask-Description task))
  (message "| Duration    : %s" (myTask-Duration task))
  (message "| Done        : %.2f%%" (if (myTask-Done task) (* 100 (myTask-Done task)) 0))
  (message "| Before      : %s" (cl-loop for B in (myTask-Before task) collect (myTask-Id B)))
  (message "| After       : %s" (cl-loop for A in (myTask-After task) collect (myTask-Id A)))
  (message "| Start off.  : %s" (seq-map (lambda (c) (if (eq c "") "-" c)) (myTask-startOffset task)))
  (message "| End off.    : %s" (seq-map (lambda (c) (if (eq c "") "-" c)) (myTask-endOffset task)))
  (message "| Parent      : %s" (if (myTask-Parent task) (myTask-Id (myTask-Parent task))  "-"))
  (message "| Kids        : %s" (seq-map (lambda (c) (if (eq c "") "-" (myTask-Id c))) (myTask-Kids task)))
  (message "| Start       : %s" (og-fmt-time (myTask-Start task)))
  (message "| End         : %s" (og-fmt-time (myTask-End task)))
  (message "| RStart      : %s" (og-fmt-time (myTask-RStart task)))
  (message "| REnd        : %s" (og-fmt-time (myTask-REnd task)))
  (message "| Schema      : %s" (myTask-Schema task))
  (message "| Hidden      : %s" (myTask-Hidden task))
  (message "| Inedx       : %s" (myTask-Index task))
  (message "===============")
  )
;;|--------------------------------------------------------------
;;|Description : format time
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-58-2021 17:58:02
;;|--------------------------------------------------------------
(defun og-fmt-time (time)
  "format time"
  (if time (ts-format (car org-time-stamp-formats) time)
    "-")
  )
;; === Time and date ===
;;|--------------------------------------------------------------
;;|Description : calculate start and end times
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 19-16-2020 09:16:32
;;|--------------------------------------------------------------
(defun og-calculate-times ()
  "calculate start and end times"
  (message "Start og-calculate-times")
  (setq og-changed t)
  (setq og-counter 0)
  (while (and og-changed (< og-counter 20))
    (setq og-changed nil)
    (cl-loop for task in og-project do
             ;; Include the eventual offsets
             (cl-loop for A being the elements of (myTask-After task) using (index i) do
                (let* ((Of (seq-elt (myTask-startOffset task) i))
                       (S1 (myTask-Start task))
                       (E2 (og-do-dateOffset (myTask-End A) Of))
                       )
                  (when (and E2 (ts< S1 E2))
                    ;; (message "offsets: set start %s from %s to %s" (myTask-Id task)
                    ;;          (og-fmt-time (myTask-Start task))
                    ;;          (og-fmt-time E2))
                    (setf (myTask-Start task) E2)
                    (setq og-changed t)
                    )
                  )
                )
             ;; (if (string= "1.4" (myTask-Id task)) (og-print-task task))
             ;; When the parent starts later than the child, move the child
             (when (and (myTask-Parent task) (ts> (myTask-Start (myTask-Parent task)) (myTask-Start task)))
               ;; (message "parent/child: set start %s from %s to %s" (myTask-Id task)
               ;;          (og-fmt-time (myTask-Start task))
               ;;          (og-fmt-time (myTask-Start (myTask-Parent task))))
               (setf (myTask-Start task) (myTask-Start (myTask-Parent task)))
               (setq og-changed t)
               )
             ;; When we have an RSTART overrule the restitution
             (if (myTask-RStart task) (setf (myTask-Start task) (myTask-RStart task)))
             ;; Make sure that the end is later than start+duration or equal to REnd
             (let ((oldEnd (myTask-End task)))
               (if (myTask-REnd task)
                   (setf (myTask-End task) (myTask-REnd task))
                 (when (> (length (myTask-Duration task)) 0)
                   (setf (myTask-End task) (og-calc-end-from-start task)))
                 (cl-loop for K in (myTask-Kids task) do
                          (when (ts> (myTask-End K) (myTask-End task))
                            (setf (myTask-End task) (myTask-End K))
                            (setq og-changed t)
                            )
                          )
                 )
               (unless (ts= oldEnd (myTask-End task)) (setq og-changed t))
               )
             )
    (cl-incf og-counter)
    ;; (message "og-changed = %s, og-counter = %s" og-changed og-counter)
    )
  (if (> og-counter 20) (error "Could not converge start/end dates. Probably a circular definition (or I gave up to soon)"))
  (cl-loop for task in og-project do
           (when (ts> (myTask-End task) og-end) (setf og-end (myTask-End task)))
           )
  (setf (myTask-End (og-get-task-from-id "Title")) og-end)
  (message "Start og-calculate-times")
  )
;;|--------------------------------------------------------------
;;|Description : Calculate percetage done
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-31-2021 18:31:41
;;|--------------------------------------------------------------
(defun og-calc-done (&optional itemList task)
  "Calculate percetage done"
  (message "Start og-calc-done")
  (let* ((items (or itemList og-project))
         (frac (cl-loop for i in items
                        collect (if (myTask-Kids i) (og-calc-done (myTask-Kids i) i) (myTask-Done i))
                        )
               )
         )
    (message "Done og-calc-done")
    (when task
      ;; (message "task = %s frac = %s" (myTask-Id task) frac)
      (setf (myTask-Done task) (min 1 (/ (apply '+ frac) (length frac))))
      )
    )
  )
  

;;|--------------------------------------------------------------
;;|Description : ofsett a timestamp by a given amount (d,w,m,y)
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-20-2020 18:20:01
;;|--------------------------------------------------------------
(defun og-do-dateOffset (date offset)
  "ofsett a timestamp by a given amount (d,w,m,y)"
  (interactive "P")
  (cond ((cl-search "y" offset) (ts-adjust 'year (string-to-number offset)  date))
        ((cl-search "m" offset) (ts-adjust 'month (string-to-number offset) date))
        ((cl-search "w" offset) (ts-adjust 'day  (* 7 (string-to-number offset)) date))
        ((cl-search "d" offset) (ts-adjust 'day  (string-to-number offset) date))
        (t date)
        )
  )
;;|--------------------------------------------------------------
;;|Description : calculate end as function from start and duration
;;|NOTE : n can be in days, weeks or months
;;|-
;;|Author : jouke hylkema
;;|date   : 19-14-2020 10:14:41
;;|--------------------------------------------------------------
;; (defun og-calc-end (task)
  ;; "calculate date n units from start"
  ;; (setf (myTask-End task) (og-calc-end-from-startq task))
  ;; )
;;|--------------------------------------------------------------
;;|Description : calculate the end date based on start and duration
;;|NOTE : duration can be in months, weeks or days
;;|-
;;|Author : jouke hylkema
;;|date   : 19-50-2020 11:50:53
;;|--------------------------------------------------------------
(defun og-calc-end-from-start (task)
  "calculate the end date based on start and duration"
  (let* ((start (myTask-Start task))
         (n (myTask-Duration task))
         (mode (cond ((cl-search "m" n) 'month)
                     (t 'day)))
         (duration (cond ((cl-search "m" n) (string-to-number (substring n 0 -1)))
                         ((cl-search "w" n) (* 7 (string-to-number (substring n 0 -1))))
                         ((cl-search "d" n) (string-to-number (substring n 0 -1)))
                         (t 0)))
         )
    ;; (message "dbg: id=%s start=%s n=%s mode=%s duration=%s end=%s"
    ;;          (myTask-Id task)
    ;;          (og-fmt-time start)
    ;;          n
    ;;          mode
    ;;          duration
    ;;          (og-fmt-time (ts-adjust mode duration start))
    ;;          )
    (ts-adjust mode duration start)
    )
  )
  ;;|--------------------------------------------------------------
  ;;|Description : get the content of a subtree
  ;;|NOTE : ²
  ;;|-
  ;;|Author : jouke hylkema
  ;;|date   : 08-44-2024 16:44:06
  ;;|--------------------------------------------------------------
  (defun og-get-content ()
    "get the content of a subtree"
    (interactive)
    (if (org-before-first-heading-p)
        (error "Not in or on an org heading")
      (save-excursion
        ;; If inside heading contents, move the point back to the heading
        ;; otherwise `org-agenda-get-some-entry-text' won't work.
        (unless (org-on-heading-p) (org-previous-visible-heading 1))
        (substring-no-properties
         (org-agenda-get-some-entry-text
          (point-marker)
          most-positive-fixnum))
        )
      )
    )

(provide 'org-gantt-parse)
;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
