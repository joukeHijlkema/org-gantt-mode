;;; org-gantt-mode.el --- Generate GANTT charts from an org-mode subtree

;; Author: Jouke Hijlkema hylkema@free.fr
;; URL: https://gitlab.com/joukeHijlkema/org-gantt
;; Version: 0.1
;; Packages-Required: ((emacs "26.1") (svg "1.0") (ts "20191010.210"))
;; Keywords: org-mode gantt svg

;; Left to do:
;; DONE: allow for an offset in the before and after lists (After T1.2+2w)
;; DONE: allow for a fixed start or end date
;; DONE: Allow for efforts in days and hence a day bar in the grid header
;; - Alow for selective output (period, max depth)
;; - Modify id, insert id
(define-minor-mode org-gantt-mode
  "Create SVG gantt charts from an org subtree"
  :lighter " Gantt")


(require 'svg)
(require 'ts)
(require 'htmlize)
(require 'cl-lib)
(require 'org)
(require 'emacsql-mysql)

(defvar og-Cols '((strokeColor  . "black")
                  (dayColor     . "purple")
                  (bgTitleBlock . "lightgray")
                  (bgGridHead   . "gray")
                  (bgTaskEven   . "lightgray")
                  (bgTaskOdd    . "gray")
                  (bgTaskLevel1 . "darkgreen")
                  (taskDone     . "red")
                  (taskDuration . "black")
                  (fillTaskLevel1 . "black")
                  (bgTask       . "blue")
                  (bgTaskP      . "darkblue")
                  (bgKP         . "purple")
                  (bgKPdone     . "green")
                  (stLink       . "red")
                  (weekend      . "peru")
                  (week         . "black")
                  )
  "default gantt colors")
(defvar og-Dims '((rowH . 21)
                  (headRows . 3)
                  (taskWidth . 60)
                  )
  "default dimensions for gantt")
(defvar og-Text '((fontSize . 20)
                  (fontFamily . "sans")
                  (sw . 0.2)
                  (dateFormat . "%d/%m/%Y"))
  "default font stuff for gantt")

(cl-defstruct myTask
  Id
  Kids
  Description
  Objective
  Type
  Parent
  Before
  After
  Start
  End
  RStart
  REnd
  Level
  Duration
  startOffset
  endOffset
  Done
  X1
  X2
  Y
  Content
  Resp
  Avec
  Schema
  Hidden
  Index
  )

(defvar og-project)
(defvar og-title)
(defvar og-start)
(defvar og-end)
(defvar og-changed)
(defvar og-counter)
(defvar og-svgPath)
(defvar og-today)
(defvar og-bg-color)
(defvar og-map)
(defvar og-printStart)
(defvar og-printEnd)
(defvar og-index)
(defvar og-levelOffset)
(defvar og-resources)

(require 'org-gantt-parse)
(require 'org-gantt-draw)
(require 'org-gantt-ui)
(require 'org-gantt-mysql)

;;|--------------------------------------------------------------
;;|Description : calculate the planning
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 25-47-2021 11:47:46
;;|--------------------------------------------------------------
(defun og-run ()
  "calculate the planning"
  (interactive)
  (save-excursion
    (setq og-map '())
    (setq og-start nil)
    (setq og-end nil)
    (setq og-printStart nil)
    (setq og-printEnd nil)
    (setq og-resources '())
    (og-parse)
    (og-fix-BA)
    (og-fix-hierarchy)
    (og-calculate-times)
    (og-calc-done)
    (unless og-printEnd (setq og-printEnd og-end))
    (message "Start og-makeGantt")
    (let ((scale (string-to-number (if (org-entry-get (point) "SCALE" t) (org-entry-get (point) "SCALE" t) "2"))))
      (og-makeGantt scale 1850 0.25)
      )
    (message "Start og-makeHTML")
    (og-makeHTMLelts 1850 0.25)
    (message "Start og-save")
    (og-save)
    (message "Start og-report")
    (og-report)
    (message "Start og-saveToSql")
    (og-saveToSql)
    ;; (og-print-project)
    (message "Gantt done")
    )
  )
;;|--------------------------------------------------------------
;;|Description : fix before after sets when using edna formats
;;|NOTE : 
;;|-
;;|Author : jouke hylkema
;;|date   : 03-14-2020 18:14:31
;;|--------------------------------------------------------------
(defun og-fix-BA-edna (task)
  "fix before after sets when using edna formats"
  (interactive "P")
  (let ((After (list))
        (AOffset (list))
        (Before (list))
        (BOffset (list)))
    (cl-loop for A
             in (myTask-After task)
             do (let* ((target (og-get-task-from-id (car (split-string A "+"))))
                       (offset (cadr (split-string A "+"))))
                  (if target (push target After))
                  (if offset (push offset AOffset) (push "0" AOffset))
                  )
             )
    (cl-loop for B
             in (myTask-Before task)
             do (let* ((target (og-get-task-from-id (car (split-string B "+"))))
                       (offset (cdr (split-string B "+"))))
                  (if target (push target Before))
                  (if offset (push offset BOffset))
                  )
             )
    (setf (myTask-After task) After)
    (setf (myTask-Before task) Before)
    (setf (myTask-startOffset task) AOffset)
    (setf (myTask-endOffset task) BOffset)
    )
  
  )
(provide 'org-gantt-mode)
;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
