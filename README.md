# org-gantt

Create SVG gantt charts from an org-mode subtree.

![Gantt chart](/Images/Gantt.png)

## Installation
Org-gantt requires these Emacs packages to run:
- htmlize
- svg
- ts
Additionally, Inkscape needs to be installed on your system.

To use the org-gantt minor mode put the org-gant-xxx.el files somewhere where emacs can find them and add
```lisp
(use-package org-gantt-mode)
```
to your init file
```lisp
M-x org-gantt-mode
```
activates the minor mode

## Usage
Have a look at the Test.org file. 
- Make sure that the first level of the planning (My fancy project) has
  -  TASK_ID equal to "Title". This is important to find the limits of the planning iinside a bigger document.
  -  SVG_PATH the place where you want your images stored SVG_PATH.svg is constructed and if inkscape is in your path you also get a SVG_PAT.pdf and SVG_PATH.pdf
  -  TODAY defines the today line
  -  BG_COLOR The background color. Ommit for transparent BG.

If you want to change the color scheme you can change
```lisp
(defvar og-Cols '((strokeColor  . "black")
                  (dayColor     . "purple")
                  (bgTitleBlock . "lightgray")
                  (bgGridHead   . "gray")
                  (bgTaskEven   . "lightgray")
                  (bgTaskOdd    . "gray")
                  (bgTaskLevel1 . "darkgreen")
                  (taskDone     . "red")
                  (taskDuration . "black")
                  (fillTaskLevel1 . "black")
                  (bgTask       . "blue")
                  (bgTaskP      . "darkblue")
                  (bgKP         . "purple")
                  (bgKPdone     . "green")
                  (stLink       . "red")
                  )
  "default gantt colors")
  ```
  in org-gantt-mode.el.
  
you can use M-x og-insert-task to add a task. When you're done (or changed anything) just run M-x og-run to create the gantt charts.

Please send me feedback since this is my first emacs mode :-)

## YASnippet
I use the following snippet to initiate my plannings
```lisp
# -*- mode: snippet -*-
# name: planning
# key: planning
# --
* ${1:Planning}
:PROPERTIES:
:Task_ID: Title
:COLUMNS: %ITEM(item) %TASK_ID(task_id) %EFFORT(eff) %BEFORE(before) %AFTER(after) %RSTART(start) %REND(end) %TYPE(type) %RESP(resp) %OBJECTIVE(objective)
:RESP_ALL: ${2:"resp1" "resp2" "resp3"}
:RSTART: ${3:`(org-read-date)`}
:SVG_PATH: ${4:`(read-file-name "svg path (no extension):")`}
:END:
Use og-insert-task to add tasks

* Charts
** Gantt
   [[file:$4.svg][Gantt]]
```

Jouke
